import axios from 'axios';
import AplicationProperties from '../../utils/AplicationProperties';

/**¿Que es axios?
 * 
Axios es un cliente HTTP basado en Promesas para Javascript,
el cual puede ser utilizado tanto en tu aplicación Front-end,
como en el Back-end con Nodejs. Utilizando Axios,
es muy sencillo enviar peticiones a endpoints REST 
y realizar operaciones CRUD
https://github.com/axios/axios
 */
export default {
/**Primero crearemos un llamado a un servicio mediante post y luego uno mediante get, Tener en cuenta que las aplicaciones Rest o Soap deberan tener 
 * permisos activados para que estas peticiones puedan lograrse --->CORS<---- 
 */
getOne(id){
    /**En el .get(Aqui va la url que configuraste para tu servicio en este caso puede ser un string o como 
     * previamente lo configuramos en un archivo asi llamarlo el dia de mañana quien sabe  si eso cambia)
     * Lo siguiente despues de la coma son los parametros que vas a envias en este caso solamente cuando es por get */
    return axios.get(AplicationProperties.dirRestService,{params:{id:id}})
    .then((response)=>{
        console.log(response.data + "Response.data es el producto de tu peticion es lo que devuelve se debe trabajar con json");
    })
    .catch((error)=>{
        console.log(error + "En caso de ocurrir un error tomamos el catch y continuamos de la manera que sea necesaria");
    })
},
create(user){
    /**aca mandamos el objeto completo en caso de java directamente te serializa este json al objeto que esta recibiendo por parametro en otros casos revisar o trabajar con json */
    /**Post y Put se manejan con objetos completos o con la data de una en caso de Delete,get hay que armar los parametros a enviar */
    return axios.post(AplicationProperties.dirRestService,user)
    .then((response)=>{
        console.log(response.data + "Response.data es el producto de tu peticion es lo que devuelve se debe trabajar con json");
    })
    .catch((error)=>{
        console.log(error + "En caso de ocurrir un error tomamos el catch y continuamos de la manera que sea necesaria");
    })
},
/**Luego ya puede ser utilizado */
getAll(){

},
update(user){

},
delete(id){

}
}