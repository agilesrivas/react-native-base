//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
// create a component
class IconComponent extends Component {
    constructor(props){
        super(props);
        this.giveTypeFontIcon=this.giveTypeFontIcon.bind(this);
    }
    giveTypeFontIcon(){
        if (this.props.font) {
            if (this.props.font === 'FontAwesome') {
                return (
                    <FontAwesome {...this.props} />
                );
            } else if (this.props.font === 'MaterialIcons') {
                return (
                    <MaterialIcons {...this.props} />
                 );
            }else if (this.props.font==='MaterialCommunityIcons'){
                return (
                    <MaterialCommunityIcons {...this.props} />
                );
            }
        }
    }
    render() {
       return (this.giveTypeFontIcon())
    }
}

// define your styles

//make this component available to the app
export default IconComponent;
