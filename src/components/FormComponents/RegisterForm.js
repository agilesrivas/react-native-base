import React, {Component} from 'react';
import {Container,Header,Footer,Content,Fab,Item,Input,Card,CardItem,Body} from 'native-base';
import {ImageBackground,View,Text, StyleSheet, Dimensions,TouchableNativeFeedback,Image} from 'react-native';
const { height_dim } = Dimensions.get('window');
export default class RegisterForm extends Component{
    constructor(props){
        super(props);
        this.state={
            name:'',
            email:'',
            username:'',
            password:'',
            encryptPass:'',
            ip:'',

        },
        this.cleanData=this.cleanData.bind(this);
        this.checkData=this.checkData.bind(this);
        this.register=this.register.bind(this);

    }
    cleanData(){
        this.setState({name:'',password:'',email:'',username:'',encryptPass:'',ip:''});
    }
    checkData(){
        if(this.state.name!==undefined || this.state.name!=='' && this.state.password!==undefined || this.state.password!=='' && this.state.email!==undefined || this.state.email!=='' && this.state.ip!==undefined || this.state.ip!=='' && this.state.username!==undefined || this.state.username!==''){
            return true;
        }
    }
    register(){
        if(this.checkData()){
            /**Loguearse */
        }
        this.cleanData();
    }
    render(){
        return ( 
            <Card transparent style={css.container}>
                <CardItem header>
                    <Text>Electro House</Text>
                </CardItem>
                <CardItem>
                    <Body>
                        <Item rounded>
                            <FormLabel>Name</FormLabel>
                            <FormInput onChangeText={(value)=>this.setState({name:value})}/>
                            <FormValidationMessage>Error name</FormValidationMessage>
                        </Item>
                        <Item rounded>
                            <FormLabel>Email</FormLabel>
                            <FormInput onChangeText={(value)=>this.setState({email:value})}/>
                            <FormValidationMessage>Error email</FormValidationMessage>
                        </Item>
                        <Item rounded>
                            <FormLabel>Username</FormLabel>
                            <FormInput onChangeText={(value)=>this.setState({username:value})}/>
                            <FormValidationMessage>Error username</FormValidationMessage>
                        </Item>
                        <Item rounded>
                            <FormLabel>Password</FormLabel>
                            <FormInput onChangeText={(value)=>this.setState({password:value})}/>
                            <FormValidationMessage>Error password</FormValidationMessage>
                        </Item>
                        <Item rounded>
                            <FormLabel>Ip</FormLabel>
                            <FormInput onChangeText={(value)=>this.setState({ip:value})}/>
                            <FormValidationMessage>Error ip</FormValidationMessage>
                        </Item>
                        <Item rounded>
                                <Button
                                raised
                                icon={{name: 'cached'}}
                                title='Register' />
                        </Item>
                    </Body>
                </CardItem>
            </Card>
        );
    }
}
const css=StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        marginTop: "30%"

    }
})