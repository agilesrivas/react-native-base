//Este es un componente que modularize
/**Importo las librerias */
import React, {Component} from 'react';
import {Fab} from 'native-base';
import { FormLabel, FormInput, FormValidationMessage,Button, Icon } from 'react-native-elements'
import {ImageBackground,View,Text, StyleSheet, Dimensions,Image,KeyboardAvoidingView } from 'react-native';
import IconComponent from '../IconComponent';

const { height_dim } = Dimensions.get('window');
/**Exporto la creacion del componente asi es como se crea uno  */
export default class LoginForm extends Component{
    /**declaro el constructor porque este componente tendra un ciclo de vida y caracteristicas propias */
    constructor(props){
        super(props);
        /**declaro el estado de este componente */
        this.state={
            name:'',
            password:'',
            status:'',
            errorName:'',
            errorPass:''
        },
        /**bindeo los metodos para poder utilizarlos en todo el componente , se puede utilizar ESMC 5 para no hacer el bind  */
        this.cleanData=this.cleanData.bind(this);
        this.checkData=this.checkData.bind(this);
        

    }
    /**Declaro las funcionalidades */
    cleanData(){
        this.setState({name:'',password:''});
    }
    checkData(){
        if(this.state.name!==undefined && this.state.name!=='' && this.state.password!==undefined && this.state.password!==''){
            this.setState({errorName:'',errorPass:''})
            return true;
        }else{
            if(this.state.name===undefined || this.state.name===''){
                this.setState({errorName:'Verifique su name'});
            }else if(this.state.password===undefined || this.state.password===''){
                this.setState({errorPass:'Verifique su contraseña'});
            }
            else{
                this.setState({error:'Incorrecto'});
            }
        }
    }
    /**o sino bindeo uso ESMC 5 */
    login=()=>{
        if(this.checkData()){
            /**Loguearse */
            this.props.navigation.navigate('Drawer');
        }
        this.cleanData();
    }

    render(){
        /**Renderizo lo que quiero mostrar */
        return ( 
        <KeyboardAvoidingView style={css.containerForm}  behavior="padding" enabled> 
            <View style={{  backgroundColor:'rgba(0,0,0,0.6)'}}>
                <FormLabel>Name</FormLabel>
                <FormInput onChangeText={(value)=>this.setState({name:value})}/>
                <FormValidationMessage>{this.state.errorName}</FormValidationMessage>
                <FormLabel>Password</FormLabel>
                <FormInput onChangeText={(value)=>this.setState({password:value})}/>
                <FormValidationMessage>{this.state.errorPass}</FormValidationMessage>
            <View style={css.fabContainer}>
            <Fab
                containerStyle={{marginLeft:30}}
                style={{ backgroundColor: '#5067FF'}}
                onPress={this.login}>
                <Icon name="send" />
                </Fab>
            </View>
                
            </View>
                
            
        </KeyboardAvoidingView>
               
            
            
        );
    }
}
/**Creo mis estilos css */
const css=StyleSheet.create({
    fabContainer:{
        paddingTop:60,
    },
    containerForm:{
        flex:1,
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
       
    }
})