//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Toolbar } from 'react-native-material-ui';
// create a component
class HeaderComponent extends Component {
    constructor(props){
        super(props)
    }
    render() {
        return (
            <View  style={styles.container}>
                 <Toolbar
                    leftElement={this.props.leftElement}
                    centerElement={this.props.title}
                    searchable={this.props.searchable}
                    rightElement={{
                        menu: {
                            icon: "more-vert",
                            labels: this.props.options
                        }
                    }}
                    onRightElementPress={ (label) => { console.log(label) }}
                />
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        marginTop:24
    },
});

//make this component available to the app
export default HeaderComponent;
