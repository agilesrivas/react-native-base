//import liraries
import React, { Component } from 'react';
import {TouchableHighlight, View, Text, StyleSheet } from 'react-native';
import IconComponent from '../IconComponent';

// create a component
class ThreePoint extends Component {
    constructor(props){
        super(props);
    }
   
    render() {
        return (
            <TouchableHighlight onPress={()=>alert("Abierto")}>
                <IconComponent  
                    name="more-vert"
                    font="MaterialIcons"
                    size={35}
                    color='black' 
                />
            </TouchableHighlight>
        );
    }
}



//make this component available to the app
export default ThreePoint;
