//import liraries
import React, { Component } from 'react';
import { TouchableHighlight , Text, StyleSheet,Icon } from 'react-native';
import IconComponent from '../IconComponent';
// create a component
class DrawerItem extends Component {
    constructor(props){
        super(props);
        this.state={
            statusDrawer:false,
        }
    }
    getActionDrawerNavigation=()=>{
        if(this.state.statusDrawer === false){
            this.setState({statusDrawer:true});
            this.props.navigation.openDrawer();

        }else{
            this.setState({statusDrawer:false});
            this.props.navigation.closeDrawer();
        } 
    }
    render() {
        return (
            <TouchableHighlight  onPress={this.getActionDrawerNavigation}>
                <IconComponent  
                    name="menu"
                    font="MaterialCommunityIcons"
                    size={35}
                    color='white'
                />
            </TouchableHighlight >
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2c3e50',
    },
});

//make this component available to the app
export default DrawerItem;
