//import liraries
import React, { Component} from 'react';
import { View, StyleSheet,ScrollView } from 'react-native';
import { Drawer} from 'react-native-material-ui';



// create a component
class CustomDrawerComponent extends Component {
    constructor(props){
        super(props);
    }
    
    render() {
        return (
            <View style={styles.container}>
                <Drawer>
        <Drawer.Header >
            
        </Drawer.Header>
        <Drawer.Section></Drawer.Section>
      </Drawer>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#2c3e50',
        marginTop:25
        
    },
});

//make this component available to the app
export default CustomDrawerComponent;
