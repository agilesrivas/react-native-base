//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet,Dimensions } from 'react-native';
import { BottomNavigation } from 'react-native-material-ui';

// create a component
const {heigth,width} = Dimensions.get('window');
class FooterComponent extends Component {
    constructor(props){
        super(props);
        this.state={
            active:''
        }
    }
    render() {
        return (
            <View style={css.container}>
               <BottomNavigation active={this.state.active} hidden={false} >
                    <BottomNavigation.Action
                        key="today"
                        icon="today"
                        label="Today"
                        onPress={() => this.setState({ active: 'today' })}
                    />
                    <BottomNavigation.Action
                        key="people"
                        icon="people"
                        label="People"
                        onPress={() => this.setState({ active: 'people' })}
                    />
                    <BottomNavigation.Action
                        key="bookmark-border"
                        icon="bookmark-border"
                        label="Bookmark"
                        onPress={() => this.setState({ active: 'bookmark-border' })}
                    />
                    <BottomNavigation.Action
                        key="settings"
                        icon="settings"
                        label="Settings"
                        onPress={() => this.setState({ active: 'settings' })}
                    />
                </BottomNavigation>
            </View>
        );
    }
}

// define your styles
const css = StyleSheet.create({
    container:{
       
        }
});

//make this component available to the app
export default FooterComponent;
