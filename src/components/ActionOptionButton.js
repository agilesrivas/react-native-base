import React, {Component} from 'react';
import { Dimensions,TouchableNativeFeedback} from 'react-native';

export default class ActionOptionButton extends Component {
	render (props) {
		return (
			<TouchableNativeFeedback >
			        {this.props.children}
			</TouchableNativeFeedback>
		);	
	}
}
