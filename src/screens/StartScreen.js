import React, {Component} from 'react';
import { StyleSheet,View,Text} from 'react-native';
import {Button} from 'native-base';

export default class StartScreen extends Component {
	constructor(props) {
		super(props);
	}
 
	render() {
        return (
            <View style={css.container}>
				<View style={css.containerLogo}>
					<Text>Aca va el logo</Text>
				</View>
				<View style={css.containerButton}>
						<Button
							onPress={()=>this.props.navigation.navigate('Login')}
							info
							rounded
							block
						>
						<Text style={{color:'white' ,fontWeight: 'bold'}}>Login</Text>
						</Button>
				</View>
				<View style={css.containerButton}>
					<Button
						onPress={()=>this.props.navigation.navigate('Register')}
						info
						block
						rounded
					>
					<Text style={{color:'white' ,fontWeight: 'bold'}}>Register</Text>
					</Button>	
				</View>	
				
         
            </View>
        );
    }
}

// define your styles
const css = StyleSheet.create({
    container: {
		flex: 1,
		marginTop:'93%',
	},
	containerLogo:{
		justifyContent: 'center',
		alignItems:'center',
		marginBottom:'20%'
	},
	buttonLogin:{
		backgroundColor:'blue',
		width:'100%'
	},
	containerButton:{
		marginBottom:15
	}
});
	
