//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import LoginForm from '../components/FormComponents/LoginForm'

// create a component
class LoginScreen extends Component {
   
    render() {
        return (
            <View style={styles.container}>
                <LoginForm navigation={this.props.navigation} />
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
    },
});

//make this component available to the app
export default LoginScreen;
