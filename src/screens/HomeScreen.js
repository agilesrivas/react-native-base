//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet,ScrollView,Dimensions } from 'react-native';
import HeaderComponent from '../components/HeaderComponents/HeaderComponent';
import DrawerItem from './../components/stackComponent/DrawerItem';
import FooterComponent from '../components/FooterComponents/FooterComponent';

// create a component
const {height} = Dimensions.get('window');
class HomeScreen extends Component {
   constructor(props){
       super(props)
   }
    render() {
        return (
            <View style={styles.container}>
                <HeaderComponent navigation={this.props.navigation} 
                title="Home" 
                options={["Option1","Option 2"]}
                searchable={
                    {autoFocus: true,
                    placeholder: 'Search'}
                  }
                leftElement={<DrawerItem navigation={this.props.navigation} />} />
                <View style={{flex:1}}>
                <ScrollView >
                    <Text style={{color:'black'}}>HomeScresen</Text>
                </ScrollView>
               
                </View>
             
                <FooterComponent style={{ flex:.2}} />
            </View>
          
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});

//make this component available to the app
export default HomeScreen;
