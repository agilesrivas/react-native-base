import {
  createStackNavigator,
  } from 'react-navigation';
  import StartScreen from '../screens/StartScreen';
  import DrawerNavigation from './stack/DrawerStack';
  import LoginScreen from '../screens/LoginScreen';
  import RegisterScreen from '../screens/RegisterScreen';

  const App = createStackNavigator({
    Start: { screen: StartScreen },
    Login: {screen:LoginScreen},
    Register:{screen:RegisterScreen},
    Drawer:{screen:DrawerNavigation},
  },
  {
    initialRouteName:'Start',
    headerMode:'none',

  });
  
  export default App;