import {
    createStackNavigator,
  } from 'react-navigation';
import React from 'react';
import DrawerScreen from './DrawerScreen';

  const DrawerNavigation=createStackNavigator({
      DrawerStack:{ screen:DrawerScreen }
  },{
    headerMode:'none'
  })
  export default DrawerNavigation;