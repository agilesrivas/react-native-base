import {createDrawerNavigator,DrawerSidebar} from 'react-navigation';
import React from 'react';
import HomeScreen from '../../screens/HomeScreen';
import ConfigurationScreen from '../../screens/ConfigurationScreen';
import CustomDrawerComponent from '../../components/stackComponent/CustomDrawerComponent';


const DrawerScreen=createDrawerNavigator({
    Home: { screen:HomeScreen },
    Configuration: { screen:ConfigurationScreen },
},
{ 
    contentComponent:CustomDrawerComponent
})
export default DrawerScreen;