import React from 'react';
import {Root} from 'native-base';
import AppNavigation from './src/navigations/AppNavigation';
export default class App extends React.Component {
  constructor(props){
    super(props);
  }
  render() {
    return (
      <Root>
        <AppNavigation/>
      </Root>
      
    );
  }
}
